# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl Business-OnlinePayment-PaySystems.t'

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use Test::More tests => 22;
use Term::ReadLine;

BEGIN { use_ok('Business::OnlinePayment::PaySystems') };

#########################

# Insert your test code below, the Test::More module is use()ed here so read
# its man page ( perldoc Test::More ) for help writing this test script.


my $term = new Term::ReadLine 'foo';
diag("Please enter a test account for PaySystems");
my $prompt = "login: ";
my $login = $term->readline($prompt);
diag("Please enter the password for the test account $login");
$prompt = "password: ";
my $pass = $term->readline($prompt);
diag("Please enter a valid creditcard to test against ");
diag("It will be charged 2 dollars ");
$prompt = "card number: ";
my $cc = $term->readline($prompt);
diag("Please enter an expiry in the form MMYY"); 
$prompt = "MMYY: ";
my $exp = $term->readline($prompt);
diag("Please enter a cvv2"); 
$prompt = "cvv2: ";
my $cvv2 = $term->readline($prompt);


diag("\n\nTrying Normal Authorization\n\n");

my $transaction;
ok($transaction = new Business::OnlinePayment('PaySystems'), 'new PaySystems');
ok($transaction->content(
                        type       => 'Visa',
                        amount     => '1.00',
                        card_number => $cc,
                        cvv2       => $cvv2,
                        expiration => $exp,
                        first_name => 'John',
                        last_name  => 'Public',
                        action     => 'NORMAL authorization',
                        login      => $login,
                        password   => $pass,
                        address    => '123 foo street',
                        city       => 'fooville',
                        state      => 'California',
                        zip        => '90210',
                        country    => 'US',
                        email      => 'foo@bar.com',
                        customer_ip => '123.123.123.123',
                        phone      => '1123342234',
                        ),
    'content');
ok($transaction->submit(), 'submit');

ok($transaction->is_success(), 'is success');

my $authcode;
ok($authcode = $transaction->authorization(), "authorization $authcode");
my $err;
ok($err = $transaction->error_message(), "error $err");
my $on;
ok($on = $transaction->order_number(), "order number $on");

diag("\n\norder number: $on auth: $authcode Error: $err\n\n");


diag("\n\nTrying Authorization Only\n\n");
ok($tx = new Business::OnlinePayment('PaySystems'), 'new PaySystems 2');
ok($tx ->content(
                        type       => 'Visa',
                        amount     => '1.00',
                        card_number => $cc,
                        cvv2       => $cvv2,
                        expiration => $exp,
                        first_name => 'John',
                        last_name  => 'Public',
                        action     => 'authorization only',
                        login      => $login,
                        password   => $pass,
                        address    => '123 foo street',
                        city       => 'fooville',
                        state      => 'California',
                        zip        => '90210',
                        country    => 'US',
                        email      => 'foo@bar.com',
                        customer_ip => '123.123.123.123',
                        phone      => '1123342234',

                       ),
    'new content');

ok($tx->submit(), 'submit');
$authcode = $err = $on = '';
ok($tx->is_success(), 'is success');
ok($authcode = $tx->authorization(), "authorization $authcode");
ok($err = $tx->error_message(), "error $err");
ok($on = $tx->order_number(), "order number $on");
my $ordernum = $on;

diag("\n\norder_number: $on auth: $authcode err: $err\n\n");


diag("\n\nTrying Post Authorization\n\n");
ok($tx2 = new Business::OnlinePayment('PaySystems'), 'new PaySystems 3');
ok($tx2->content( 
                        amount     => '1.00',
                        action     => 'post authorization',
                        login      => $login,
                        password   => $pass,
                        order_number => $ordernum,
                       ),
    'new content');
ok($tx2->submit, 'submit');
$authcode = $err = $on = '';
ok($tx2->is_success(), 'is success');
ok($authcode = $tx2->authorization(), "authorization $authcode");
ok($err = $tx2->error_message(), "error $err");
ok($on = $tx2->order_number(), "order number $on");

diag("\n\norder_number: $on auth: $authcode err: $err\n\n");

